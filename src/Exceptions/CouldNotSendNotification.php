<?php
declare(strict_types=1);

namespace YouStock\NotificationChannels\Ringover\Exceptions;

class CouldNotSendNotification extends \RuntimeException
{
}
