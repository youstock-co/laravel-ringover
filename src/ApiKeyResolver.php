<?php
declare(strict_types=1);

namespace YouStock\NotificationChannels\Ringover;

interface ApiKeyResolver
{
    public function resolve(string $phoneNumber): string;
}
